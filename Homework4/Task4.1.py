import random
import cv2

img = cv2.imread('lena.jpg')


def add_noise(img):
    # Getting the dimensions of the image
    row, col = img.shape

    # Randomly pick some pixels in the
    # image for coloring them white
    # Pick a random number between 300 and 10000
    #number_of_pixels = random.randint(300, 10000)

    # for 10% = 4500, for 50% = 22500 , for 90% = 40500
    print("Enter the percent of corruption:")
    percent_of_corruption = int(input())
    number_of_pixels = int(300*300*percent_of_corruption/100/2)

    for i in range(number_of_pixels):
        # Pick a random y coordinate
        y_coord = random.randint(0, row - 1)

        # Pick a random x coordinate
        x_coord = random.randint(0, col - 1)

        # Color that pixel to white
        img[y_coord][x_coord] = 255

    # Randomly pick some pixels in
    # the image for coloring them black
    # Pick a random number between 300 and 10000
    # number_of_pixels = random.randint(300, 10000)
    for i in range(number_of_pixels):
        # Pick a random y coordinate
        y_coord = random.randint(0, row - 1)

        # Pick a random x coordinate
        x_coord = random.randint(0, col - 1)

        # Color that pixel to black
        img[y_coord][x_coord] = 0

    return img


# salt-and-pepper noise can
# be applied only to grayscale images
# Reading the color image in grayscale image
img = cv2.imread('lena.jpg',
                 cv2.IMREAD_GRAYSCALE)
cv2.namedWindow('Normal Lena')
cv2.imshow('Normal Lena', img)

salted_img = add_noise(img)
cv2.imshow('Salted Lena', salted_img)

averaging = cv2.blur(src=salted_img, ksize=(5, 5))
cv2.imshow("Averaging Blurred", averaging)

median = cv2.medianBlur(src=salted_img, ksize=5)
cv2.imshow("Median Blurred", median)

gaussian = cv2.GaussianBlur(src=salted_img, ksize=(5, 5), sigmaX=0, sigmaY=0)
cv2.imshow("Gaussian Blurred", gaussian)

bilateral_filter = cv2.bilateralFilter(src=salted_img, d=9, sigmaColor=75, sigmaSpace=75)
cv2.imshow("Bilateral Filtering", bilateral_filter)

# Storing the image
# cv2.imwrite('salt-and-pepper-lena.jpg', add_noise(img))
cv2.waitKey(0)
