import random
import cv2
import np as np
from matplotlib import pyplot as plt

img = cv2.imread('lena.jpg')
value: int = 0
salted_img = img.copy()

def onValueChange(args):
    global value, img, salted_img
    value = args
    cv2.destroyWindow("Median Blurred")#"Gaussian Smoothing","Mean Filtered Image",'Salted Lena')
    cv2.destroyWindow("Gaussian Smoothing")
    cv2.destroyWindow("Mean Filtered Image")
    cv2.destroyWindow('Salted Lena')
    salted_img = add_noise(img)
    cv2.imshow('Salted Lena', salted_img)
    printFrequencyFilter(salted_img)

def add_noise(img):
    # Getting the dimensions of the image
    global value
    row, col = img.shape
    # for 10% = 4500, for 50% = 22500 , for 90% = 40500
    percent_of_corruption = value
    print(value)
    number_of_pixels = int(300*300*percent_of_corruption/100/2)
    print(number_of_pixels)
    for i in range(number_of_pixels):
        # Pick a random y coordinate
        y_coord = random.randint(0, row - 1)

        # Pick a random x coordinate
        x_coord = random.randint(0, col - 1)

        # Color that pixel to white
        img[y_coord][x_coord] = 255

    for i in range(number_of_pixels):
        # Pick a random y coordinate
        y_coord = random.randint(0, row - 1)

        # Pick a random x coordinate
        x_coord = random.randint(0, col - 1)

        # Color that pixel to black
        img[y_coord][x_coord] = 0

    return img

def printFrequencyFilter(img):
    median = cv2.medianBlur(img,5)
    cv2.imshow("Median Blurred", np.hstack((img, median)))

    gaussBlur = cv2.GaussianBlur(img, (5, 5), cv2.BORDER_DEFAULT)
    cv2.imshow("Gaussian Smoothing", np.hstack((img, gaussBlur)))

    kernel = np.ones((10, 10), np.float32) / 25
    meanFilter = cv2.filter2D(img, -1, kernel)
    cv2.imshow("Mean Filtered Image", np.hstack((img, meanFilter )))


# salt-and-pepper noise can
# be applied only to grayscale images
# Reading the color image in grayscale image
img = cv2.imread('lena.jpg',
                 cv2.IMREAD_GRAYSCALE)
cv2.namedWindow('Normal Lena')
cv2.createTrackbar("Value","Normal Lena",value,100,onValueChange)
cv2.imshow('Normal Lena', img)

cv2.waitKey(0)

# loading image
#img0 = cv2.imread('SanFrancisco.jpg',)
img0 = cv2.imread('lena.jpg',)

# converting to gray scale
gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)

# remove noise
img = cv2.GaussianBlur(gray,(3,3),0)

# convolute with proper kernels
laplacian = cv2.Laplacian(img,cv2.CV_64F)
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)  # x
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)  # y

plt.subplot(2,2,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian,cmap = 'gray')
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])

plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
