import math

import cv2
import cv2 as cv
import numpy as np

image = cv.imread('map.png')
points = []
pathLenght: float = 0
scale: float = 1.12359551
endDrawing: bool = False


def distance(a, b):
    return math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]))


def draw_circle(event, x, y, flags, param):
    global pathLenght, image

    if endDrawing:
        return

    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(image, (x, y), 3, (0, 255, 0),-1,cv2.FILLED)
        points.append((x, y))

        if len(points) > 1:
            cv2.line(image, points[len(points) - 1], points[len(points) - 2], (0, 0, 255),2)
            pathLenght += distance(points[len(points) - 1], points[len(points) - 2])


cv2.namedWindow('Map')
cv2.setMouseCallback('Map', draw_circle)
while True:
    cv2.imshow('Map', image)
    key = cv2.waitKey(1) & 0xFF

    if key == ord('s'):
        if not endDrawing:
            cv2.putText(image, f"{int(pathLenght * scale)} meter", (50, 50),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 100, 114), 2, cv2.LINE_AA)
            endDrawing = True
    if key == ord('q'):
        cv2.destroyAllWindows()
        break

cv2.waitKey(0)