import time

import cv2
import duration as duration
import numpy as np

import cv2

cap = cv2.VideoCapture('pizza.mp4')
fps = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
duration = frame_count / fps
print('fps = ' + str(fps))
print('number of frames = ' + str(frame_count))
print('duration (S) = ' + str(duration))
minutes = int(duration / 60)
seconds = duration % 60
print('duration (M:S) = ' + str(minutes) + ':' + str(seconds))
stop_time = 10
print(int(duration))
start_time = time.time()
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter('output.mp4', fourcc, fps, (int(cap.get(3)), int(cap.get(4))))

def annotation(frame, now, start, end, text):
    if start < now < end:
        cv2.putText(frame,
                    text,
                    (442, 432),
                    font, 1,
                    (0, 255, 255),
                    2,
                    cv2.LINE_4)


while (cap.isOpened()):

    # Capture frames in the video
    ret, frame = cap.read()

    # describe the type of font
    # to be used.
    font = cv2.FONT_HERSHEY_SIMPLEX
    # Use putText() method for
    # inserting text on video
    now = time.time()
    secnow = (now - start_time) % 60
    print(secnow)

    # ANNOTATION(FRAME,SECNOW, START_SEC, END_SEC, TEXT)
    annotation(frame, secnow, 3, 7, 'adding ingredients')
    annotation(frame, secnow, 14, 23, "'PIZZA MAKING'")

    # Display the resulting frame
    time.sleep(1 / fps)
    cv2.imshow('Pizza', frame)
    out.write(frame)
    # creating 'q' as the quit
    # button for the video
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# release the cap object

cap.release()
out.release()
# close all windows
cv2.destroyAllWindows()

