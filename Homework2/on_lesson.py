import cv2
import cv2 as cv
import numpy as np

image = cv.imread('IMAGE.jpeg')

height, width = image.shape[:2]
center=(width/2,height/2)

#Rotation
# rotate_matrix=cv2.getRotationMatrix2D(center=center,angle=45,scale=1)
# print(rotate_matrix)
# rotated_image=cv2.warpAffine(src=image,M=rotate_matrix,dsize=(width,height))
# cv2.imshow('Original',image)
# cv2.imshow('Rotated',rotated_image)

#Translation
# tx,ty = width/4,height/4
#
# translation_matrix = np.array([
#     [1,0,tx],
#     [0,1,ty]
# ], dtype=np.float32)
# translated_image = cv2.warpAffine(src=image,M=translation_matrix,dsize=(width,height))
#
# cv2.imshow('Original',image)
# cv2.imshow('Translated',translated_image)

#Annotation
# cv2.imshow('Original',image)
#
# if image is None:
#     print('Could not read image')
# imageLine=image.copy()
# #Draw the image from point A to B
# pointA=(200,80)
# pointB=(450,80)
# cv2.line(imageLine,pointA,pointB,(255,255,0),thickness=3,lineType=cv2.LINE_AA)
# cv2.imshow('Image Line',imageLine)

#Shapes & texts
img_to_draw = np.zeros((512,512,3), np.uint8)
print(img_to_draw.shape)
img_to_draw[100:200, 300:400]=255,0,0
cv2.line(img_to_draw,(0,0),(img_to_draw.shape[1],img_to_draw.shape[0]),(255,0,255),3)
cv2.rectangle(img_to_draw,(0,0),(200,300),(0,255,255),cv2.FILLED)
cv2.circle(img_to_draw,(400,50),40,(0,255,255),5)
cv2.putText(img_to_draw," OPENCV ",(300,100),cv2.FONT_HERSHEY_SIMPLEX,1.5,(0,150,0),2)
cv2.imshow('Image Line',img_to_draw)
cv2.waitKey(0)
