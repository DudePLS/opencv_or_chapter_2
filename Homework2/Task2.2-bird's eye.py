import cv2
import numpy as np

start = False
# Create a video capture object, in this case we are reading the video from a file
vid_capture = cv2.VideoCapture('pizza.mp4')
if (vid_capture.isOpened() == False):
    print("Error opening the video file")
# Read fps and frame count
else:
    # Get frame rate information
    # You can replace 5 with CAP_PROP_FPS as well, they are enumerations
    fps = vid_capture.get(5)
    print('Frames per second : ', fps, 'FPS')

    # Get frame count
    # You can replace 7 with CAP_PROP_FRAME_COUNT as well, they are enumerations
    frame_count = vid_capture.get(7)
    print('Frame count : ', frame_count)

while True:
    # vid_capture.read() methods returns a tuple, first element is a bool
    # and the second is frame
    ret, frame = vid_capture.read()
    cv2.imshow('Frame', frame)
    if ret == True:
        # 20 is in milliseconds, try to increase the value, say 50 and observe
        key = cv2.waitKey(20)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:# ESC butto
            break
        if key == ord('g'):
            start = True
        if start:
            width, height = vid_capture.get(3), vid_capture.get(4)


            # All points are in format [cols, rows]
            pt_A = [52, 280]
            pt_B = [268, 476]
            pt_C = [847, 285]
            pt_D = [565, 118]

            # Here, I have used L2 norm. You can use L1 also.
            width_AD = np.sqrt(((pt_A[0] - pt_D[0]) ** 2) + ((pt_A[1] - pt_D[1]) ** 2))
            width_BC = np.sqrt(((pt_B[0] - pt_C[0]) ** 2) + ((pt_B[1] - pt_C[1]) ** 2))
            maxWidth = max(int(width_AD), int(width_BC))

            height_AB = np.sqrt(((pt_A[0] - pt_B[0]) ** 2) + ((pt_A[1] - pt_B[1]) ** 2))
            height_CD = np.sqrt(((pt_C[0] - pt_D[0]) ** 2) + ((pt_C[1] - pt_D[1]) ** 2))
            maxHeight = max(int(height_AB), int(height_CD))

            input_pts = np.float32([pt_A, pt_B, pt_C, pt_D])
            output_pts = np.float32([[0, 0],
                                     [0, maxHeight - 1],
                                     [maxWidth - 1, maxHeight - 1],
                                     [maxWidth - 1, 0]])
            M = cv2.getPerspectiveTransform(input_pts, output_pts)
            out = cv2.warpPerspective(frame, M, (maxWidth, maxHeight), flags=cv2.INTER_LINEAR)

            cv2.imshow("Out", out)
        if key == ord('q'):
            break
    else:
        break

# Release the video capture object
vid_capture.release()
cv2.destroyAllWindows()
